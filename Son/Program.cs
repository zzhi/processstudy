﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;

namespace Son
{
    class Program
    {
        static void Main(string[] args)
        {
            //Random r = new Random();
            //Console.WriteLine(r.Next(1,10000));
            //Console.Read();

            FatherProcessId();
        }
        public  static void FatherProcessId()
        {
            var myId = Process.GetCurrentProcess().Id;
            var query = string.Format("SELECT ParentProcessId FROM Win32_Process WHERE ProcessId = {0}", myId);
            var search = new ManagementObjectSearcher("root\\CIMV2", query);
            var results = search.Get().GetEnumerator();
            results.MoveNext();
            var queryObj = results.Current;
            var parentId = (uint)queryObj["ParentProcessId"];
            var parent = Process.GetProcessById((int)parentId);
            Console.WriteLine("I was started by {0},Process Id={1}", parent.ProcessName,parent.Id);
            Console.ReadLine();
        }
    }
}
