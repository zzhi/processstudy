﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IPCClient
{
    class Program
    {
        static void Main(string[] args)
        {
            //启动当前进程
            Application.Run(new AppContext(args[0]));
        }
    }
}
