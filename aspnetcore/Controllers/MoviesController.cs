﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Dapper;
using aspnetcore.Models;
// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace aspnetcore.Controllers
{
    public class MoviesController : Controller
    {
        string server = "server=127.0.0.1;database=test;uid=root;pwd=QAZwsx123;charset='gbk'";
        // GET: Movies
        public IActionResult Index()
        {
            MySqlConnection con = new MySqlConnection(server);
            //查询数据
            var movies = con.Query<Movie>("select * from movie");

            return View(movies.ToList());
        }
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            MySqlConnection con = new MySqlConnection(server);
            var movie = con.Query<Movie>("select * from movie").SingleOrDefault(m => m.ID == id);
            if (movie == null)
            {
                return NotFound();
            }

            return View(movie);
        }
    }
}
