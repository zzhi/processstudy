﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IPCServer
{
    /// <summary>
    /// Inter-Process Communication进程间通信
    /// </summary>
    class Program
    {
        static List<ProcessHost> mProcesesList = new List<ProcessHost>();
        static void Main(string[] args)
        {
            Console.WriteLine("////////////////////////////////////////////");
            Console.WriteLine("\t \t 按 Enter 停止 ");
            Console.WriteLine("////////////////////////////////////////////");
            Console.WriteLine("\n\n");

            LoadApps();

            Console.ReadLine();

            UnloadApps();

            Console.WriteLine("\n\n");
            Console.WriteLine("Press Enter to exit");
            Console.ReadLine();

        }
        /// <summary>
        /// 启动客户端
        /// </summary>
        static void LoadApps()
        {
            for (int i = 1; i <= 3; i++)
            {
                ProcessHost p = new ProcessHost();
                p.Start("IPC_Client" + i.ToString());
                
                mProcesesList.Add(p);
            }
        }
        static void UnloadApps()
        {
            foreach (ProcessHost p in mProcesesList)
            {
                p.Dispose();
            }
        }
    }
}
