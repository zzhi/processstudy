﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IPCClient
{
    public class AppContext : ApplicationContext
    {
        private string mId;
        private NamedPipeClientStream mPipeClientStream;//命名管道
        private System.Timers.Timer mKeepAliveTimer = new System.Timers.Timer(2000);

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="paramID"></param>
        public AppContext(string paramID)
        {
            //退出事件
            Application.ApplicationExit += new EventHandler(Application_ApplicationExit);
            mId = paramID;

            StartIPC();

            mKeepAliveTimer.Elapsed += new System.Timers.ElapsedEventHandler(m_KeepAliveTimer_Elapsed);
            mKeepAliveTimer.Interval = 2000;
            mKeepAliveTimer.Start();
        }

        /// <summary>
        /// Sending message to the parent application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void m_KeepAliveTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                if (mPipeClientStream.IsConnected)
                {
                    string msg = "智哥，你好。";
                    StreamWriter sw = new StreamWriter(mPipeClientStream);
                    sw.WriteLine("来自"+ mId+"的消息："+msg);
                    
                    sw.Flush();
                    System.Diagnostics.Trace.WriteLine("KeepAlive was sent by " + mId);
                }

            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("pipeClient is disonnected for " + mId + "Error: " + ex.ToString());
                Application.Exit();
            }

        }

        /// <summary>
        /// 进程退出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Application_ApplicationExit(object sender, EventArgs e)
        {
            System.Diagnostics.Trace.WriteLine("Application Exit " + mId);
        }

        /// <summary>
        /// Connectin to the IPC server
        /// </summary>
        private void StartIPC()
        {
            System.Diagnostics.Trace.WriteLine("Starting IPC client for " + mId);
            mPipeClientStream = new NamedPipeClientStream(".",
                mId,
                PipeDirection.InOut,
                PipeOptions.Asynchronous);

            try
            {
                mPipeClientStream.Connect(3000);
                System.Diagnostics.Trace.WriteLine("Connected to IPC server. " + mId);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine("StartIPC Error for " + ex.ToString());
                return;
            }
        }
    }
}
