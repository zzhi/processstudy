﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IPCServer
{
    /// <summary>
    /// 管理子进程生命周期
    /// </summary>
    public  class ProcessHost : IDisposable
    {
        public const int BufferSize = 1024 * 2;
        private string mPipeId;
        private Process mChildProcess;
        private NamedPipeServerStream mPipeServerStream;
        private bool mIsDisposing;
        private Thread mPipeMessagingThread;

        /// <summary>
        /// 构造函数
        /// </summary>
        public ProcessHost()
        {

        }

        /// <summary>
        /// 启动ipc服务并运行子进程
        /// </summary>
        /// <param name="paramUid">Unique ID of the named pipe</param>
        /// <returns></returns>
        public bool Start(string paramUid)
        {
            mPipeId = paramUid;

            mPipeMessagingThread = new Thread(new ThreadStart(StartIPCServer));
            mPipeMessagingThread.Name = this.GetType().Name + ".PipeMessagingThread";
            mPipeMessagingThread.IsBackground = true;
            mPipeMessagingThread.Start();

            ProcessStartInfo processInfo = new ProcessStartInfo("IPCClient", this.mPipeId);
            mChildProcess = Process.Start(processInfo);
           

            return true;

        }
        /// <summary>
        ///  Send message to the child process
        /// </summary>
        /// <param name="paramData"></param>
        /// <returns></returns>
        public bool Send(string paramData)
        {
            return true;
        }

        /// <summary>
        /// Start the IPC server listener and wait for
        /// incomming messages from the appropriate child process
        /// </summary>
        private void StartIPCServer()
        {
            if (mPipeServerStream == null)
            {
                mPipeServerStream = new NamedPipeServerStream(mPipeId,
                    PipeDirection.InOut,
                    1,
                    PipeTransmissionMode.Byte,
                    PipeOptions.Asynchronous,
                    BufferSize,
                    BufferSize);

            }

            // Wait for a client to connect
            Console.WriteLine(string.Format("{0}:Waiting for child process connection...", mPipeId));
            try
            {
                //Wait for connection from the child process
                mPipeServerStream.WaitForConnection();
                Console.WriteLine(string.Format("Child process {0} is connected.", mPipeId));
            }
            catch (ObjectDisposedException exDisposed)
            {
                Console.WriteLine(string.Format("StartIPCServer for process {0} error: {1}", this.mPipeId,
                    exDisposed.Message));
            }
            catch (IOException exIO)
            {
                Console.WriteLine(string.Format("StartIPCServer for process {0} error: {1}", this.mPipeId, exIO.Message));
            }

            bool retRead = true;
            ;
            while (retRead && !mIsDisposing)
            {
                retRead = StartAsyncReceive();
                Thread.Sleep(30);
            }
        }

        /// <summary>
        /// Read line of text from the connected client
        /// </summary>
        /// <returns>return false on pipe communication exception</returns>
        private bool StartAsyncReceive()
        {
            StreamReader sr = new StreamReader(mPipeServerStream);
            try
            {
                string str = sr.ReadLine();

                if (string.IsNullOrEmpty(str))
                {
                    // The client is down
                    return false;
                }

                Console.WriteLine(string.Format("{0}: Received: {1}. (Thread {2})", mPipeId, str,
                    Thread.CurrentThread.ManagedThreadId));

            }
            // Catch the IOException that is raised if the pipe is broken
            // or disconnected.
            catch (Exception e)
            {
                Console.WriteLine("AsyncReceive ERROR: {0}", e.Message);
                return false;
            }

            return true;

        }

        /// <summary>
        /// Dispose the client process
        /// </summary>
        private void DisposeClientProcess()
        {
            try
            {
                mIsDisposing = true;
                try
                {
                    //I will fails if the process doesn't exist
                    mChildProcess.Kill();
                }
                catch
                {
                   
                }
                mPipeServerStream.Dispose(); //This will stop any pipe activity

                Console.WriteLine(string.Format("Process {0} is Closed", mPipeId));
            }
            catch (Exception ex)
            {
                Console.WriteLine(string.Format("Process {0} is Close error: {1}", this.mPipeId, ex.Message));
            }

        }

        #region IDisposable Members

        public void Dispose()
        {
            DisposeClientProcess();
        }

        #endregion
    }

    /// <summary>
    /// 参数
    /// </summary>
    public class ReceivedEventArg : EventArgs
    {
        public string Data;

        public ReceivedEventArg(string paramData)
        {
            this.Data = paramData;
        }
    }
}
