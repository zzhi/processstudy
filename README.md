上次内部分享遗留了一个问题：进程之间如何通信？ 基于此，做了几个简单demo

1，Father项目打开Son项目，在Son项目中通过一个静态方法-FatherProcessId()找到Father进程的相关信息。
2，不同于FatherProcessId()方法，GetProcessId项目通过另一种方案获取父进程信息。
3，IPCServer和GetProcessId演示了进程间通过命名管道通信。
